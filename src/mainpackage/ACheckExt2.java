package mainpackage;

import java.util.ArrayList;
import java.util.Iterator;

/*Класс для проверки статуса - обработан*/

public class ACheckExt2 extends ACheck implements Runnable {

    @Override
    public void run() {
        checkOrders();
    }

    @Override
    void checkOrders() {
        while (true) {
            synchronized (orders) {
                ArrayList<Order> list = orders.getOrdersCart();
                Iterator<Order> iterator = list.iterator();
                while (iterator.hasNext()) {
                    Order order = iterator.next();
                    if (order.isOrderStatus()) {
                        iterator.remove();  // удаляем элемент из списка заказов - статус которого обработан (true);
                    }
                }
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ACheckExt2(Orders<Order> orders) {
        this.orders = orders;
    }
}

package mainpackage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Order {

    private ShoppingCart shoppingCart;
    private Credentials credentials;
    private boolean orderStatus;
    private long timeOfCreation;
    private long waitingTime;

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public boolean isOrderStatus() {
        return orderStatus;
    }

    public long getTimeOfCreation() {
        return timeOfCreation;
    }

    public long getWaitingTime() {
        return waitingTime;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public void setOrderStatus(boolean orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setTimeOfCreation(long timeOfCreation) {
        this.timeOfCreation = timeOfCreation;
    }

    public void setWaitingTime(long waitingTime) {
        this.waitingTime = waitingTime;
    }
}

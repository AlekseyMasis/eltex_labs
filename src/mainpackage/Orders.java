package mainpackage;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class Orders <E extends Order> {

    private volatile ArrayList<E> ordersCart = new ArrayList<>();

    public void makeAPurchase(ShoppingCart shoppingCart, Credentials credentials) {
        Order order = new Order();
        order.setTimeOfCreation(new Date().getTime());
        order.setCredentials(credentials);
        order.setShoppingCart(shoppingCart);
        order.setWaitingTime(order.getTimeOfCreation() + new Random().nextInt(20));
        ordersCart.add((E)order);
    }

    public void findToRemove() {
        for (int i = 0; i < ordersCart.size(); i++) {
            E temp = ordersCart.get(i);
            if (temp.getWaitingTime() < new Date().getTime() && temp.isOrderStatus()) {
                ordersCart.remove(i);
            }
        }
    }

    public ArrayList<E> getOrdersCart() {
        return ordersCart;
    }

    public void showAllOrders() {
        synchronized (ordersCart) {
            ordersCart.forEach(System.out::println);
//            for (Order elem: ordersCart) {
//                System.out.println(elem);
//            }
        }
    }
}

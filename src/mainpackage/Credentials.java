package mainpackage;

import java.util.UUID;

public class Credentials {
    private UUID id;
    private String firstName;
    private String lastdName;
    private String  patronymic;
    private String email;

    public Credentials(String firstName, String lastName, String patronymic, String email) {
        this.firstName = firstName;
        this.lastdName = lastName;
        this.patronymic = patronymic;
        this.email = email;
        id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastdName() {
        return lastdName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setId(UUID id) {
        id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastdName(String lastdName) {
        this.lastdName = lastdName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

package mainpackage;

import mainpackage.devices.Phones;
import mainpackage.devices.Smartphones;
import mainpackage.devices.Tablets;

/*класс генерации заказов*/

public class OrderGeneration implements Runnable {
    private Orders<Order> orders;

    @Override
    public void run() {
        synchronized (orders) {
            for (int i = 0; i < 3; i++) { // создаем корзину с покупками добавляем в неё три разных объекта
                ShoppingCart<ElectronicDevice> shoppingCart = new ShoppingCart<>();
                Phones phones = new Phones();
                phones.create();
                shoppingCart.add(phones);
                Smartphones smartphones = new Smartphones();
                smartphones.create();
                shoppingCart.add(smartphones);
                Tablets tablets = new Tablets();
                tablets.create();
                shoppingCart.add(tablets);
                Credentials credentials = new Credentials(phones.randomString(), phones.randomString(), phones.randomString(), phones.randomString()); // заполняем данные покупалетя случайными словами
                orders.makeAPurchase(shoppingCart, credentials); // оформляем покупку, создаем новый заказ
            }
        }
    }
    public OrderGeneration(Orders<Order> orders) {
        this.orders = orders;
    }
}

package mainpackage.devices;

import mainpackage.ElectronicDevice;


import java.util.Scanner;

public class Smartphones extends ElectronicDevice {
    private String simCardsType;
    private int simCardsCount;

    @Override
    public void create() {
        simCardsType = randomString();
        simCardsCount = randomInt();
        devName = randomString();
        devPrice = randomInt();
        devCompany = randomString();
        devModel = randomString();
        typeOfDevOS = randomString();
        devCount++;
    }

    @Override
    public void read() {
        System.out.printf("%s%n%d%n%s%n%d%n%s%n%s%n%s%n",simCardsType, simCardsCount, devName, devPrice, devCompany, devModel, typeOfDevOS);
    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
            simCardsType = scanner.nextLine();
            simCardsCount = Integer.parseInt(scanner.nextLine());
            devName = scanner.nextLine();
            devPrice = Integer.parseInt(scanner.nextLine());
            devCompany = scanner.nextLine();
            devModel = scanner.nextLine();
            typeOfDevOS = scanner.nextLine();
    }

    @Override
    public void delete() {
        simCardsType = null;
        simCardsCount = 0;
        devName = null;
        devPrice = 0;
        devCompany = null;
        devModel = null;
        typeOfDevOS = null;
        devCount--;
    }
}

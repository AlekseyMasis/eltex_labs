package mainpackage.devices;

import mainpackage.ElectronicDevice;

import java.util.Scanner;

public class Tablets extends ElectronicDevice {
    private String graphicProcessor;
    private String screenResolution;

    @Override
    public void create() {
        graphicProcessor = randomString();
        screenResolution = randomString();
        devName = randomString();
        devPrice = randomInt();
        devCompany = randomString();
        devModel = randomString();
        typeOfDevOS = randomString();
        devCount++;
    }

    @Override
    public void read() {
        System.out.printf("%s%n%s%n%s%n%d%n%s%n%s%n%s%n",graphicProcessor, screenResolution, devName,  devPrice, devCompany, devModel, typeOfDevOS);
    }

    @Override
    public void update() {
            Scanner scanner = new Scanner(System.in);
            graphicProcessor = scanner.nextLine();
            screenResolution = scanner.nextLine();
            devName = scanner.nextLine();
            devPrice = Integer.parseInt(scanner.nextLine());
            devCompany = scanner.nextLine();
            devModel = scanner.nextLine();
            typeOfDevOS = scanner.nextLine();
    }

    @Override
    public void delete() {
        graphicProcessor = null;
        screenResolution = null;
        devName = null;
        devPrice = 0;
        devCompany = null;
        devModel = null;
        typeOfDevOS = null;
        devCount--;
    }
}

package mainpackage.devices;

import mainpackage.ElectronicDevice;

import java.util.Scanner;

public class Phones extends ElectronicDevice {
    private String devCase;

    @Override
    public void create() {
        devCase = randomString();
        devName = randomString();
        devPrice = randomInt();
        devCompany = randomString();
        devModel = randomString();
        typeOfDevOS = randomString();
        devCount++;
    }

    @Override
    public void read() {
        System.out.printf("%s%n%s%n%d%n%s%n%s%n%s%n",devCase, devName, devPrice, devCompany, devModel, typeOfDevOS);
    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        devCase = scanner.nextLine();
        devName = scanner.nextLine();
        devPrice = Integer.parseInt(scanner.nextLine());
        devCompany = scanner.nextLine();
        devModel = scanner.nextLine();
        typeOfDevOS = scanner.nextLine();
    }

    @Override
    public void delete() {
        devCase = null;
        devName = null;
        devPrice = 0;
        devCompany = null;
        devModel = null;
        typeOfDevOS = null;
        devCount--;
    }
}

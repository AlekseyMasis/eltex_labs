package mainpackage;

/*Класс для проверки статуса - ожидание*/

import java.util.ArrayList;

public class ACheckExt1 extends ACheck implements Runnable {

    @Override
    public void run() {
        checkOrders();
    }

    @Override
    void checkOrders() {
        while (true) {
            synchronized (orders) {
                ArrayList<Order> list = orders.getOrdersCart();
                list.stream().filter(elem -> !elem.isOrderStatus()).forEach(elem -> elem.setOrderStatus(true)); // устанавливаем статус в обработан(true)
            }
            try {
                Thread.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public ACheckExt1(Orders<Order> orders) {
        this.orders = orders;
    }
}

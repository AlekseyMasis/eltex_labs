package mainpackage;

import mainpackage.devices.Phones;

public class Application {
    public static void main(String[] args) {
//        int objectCounts = Integer.valueOf(args[0]);
//        String viewOfObject = args[1];
//        ElectronicDevice electronicDevice = null;
//        ShoppingCart shoppingCart = new ShoppingCart();
//
//        switch (viewOfObject) {
//            case ("Phones") : {
//                for (int i = 0; i < objectCounts; i++) {
//                    electronicDevice = new Phones();
//                    electronicDevice.create();
//                    electronicDevice.update();
//                    electronicDevice.read();
//                    shoppingCart.add(electronicDevice);
//
//                }
//                break;
//            }
//            case ("Smartphones") : {
//                for (int i = 0; i < objectCounts; i++) {
//                    electronicDevice = new Phones();
//                    electronicDevice.create();
//                    electronicDevice.update();
//                    electronicDevice.read();
//                    shoppingCart.add(electronicDevice);
//                }
//                break;
//            }
//            case ("Tablets") : {
//                for (int i = 0; i < objectCounts; i++) {
//                    electronicDevice = new Phones();
//                    electronicDevice.create();
//                    electronicDevice.update();
//                    electronicDevice.read();
//                    shoppingCart.add(electronicDevice);
//                }
//            }
//        }
//        shoppingCart.showAllObjects(); // выводим все объекты в корзине
//
//        System.out.println(shoppingCart.searchByID(electronicDevice.getDevId()));
//
//        shoppingCart.delete(electronicDevice);
//
//        shoppingCart.showAllObjects();
//
//        Credentials credentials = new Credentials("firstName", "lastName", "patronymic", "email");
//
//        Orders orders = new Orders();
//
//        orders.makeAPurchase(shoppingCart, credentials);
//
//        orders.showAllOrders();
//
//        orders.findToRemove();
//
//        orders.showAllOrders();

        Orders<Order> orders = new Orders<>();

        OrderGeneration orderGeneration = new OrderGeneration(orders);
        Thread threadGeneration1 = new Thread(orderGeneration);
        Thread threadGeneration2 = new Thread(orderGeneration);
        Thread threadGeneration3 = new Thread(orderGeneration);
        threadGeneration1.start();
        threadGeneration2.start();
        threadGeneration3.start();
//        try {
//            threadGeneration1.join();
//            threadGeneration2.join();
//            threadGeneration3.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        orders.showAllOrders(); // выводим в консоль добавленные заказы

        ACheckExt1 aCheckExt1 = new ACheckExt1(orders); // меняем статус заказов на обработан

        ACheckExt2 aCheckExt2 = new ACheckExt2(orders); // удаляем из корзины с заказами все заказы статус которых - обработан

        Thread checkStatus1 = new Thread(aCheckExt1);
        checkStatus1.setDaemon(true);
        checkStatus1.start();

        Thread checkStatus2 = new Thread(aCheckExt2);
        checkStatus2.setDaemon(true);
        checkStatus2.start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("--------------------------");
        orders.showAllOrders(); // количество заказов через 3 сек (скорее всего все заказы к этому времени должны быть обработаны и удалены)

    }

}


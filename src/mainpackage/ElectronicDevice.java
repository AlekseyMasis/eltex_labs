package mainpackage;

import java.util.Random;
import java.util.UUID;

public abstract class ElectronicDevice implements ICrudAction {
    private UUID devId;
    protected String devName;
    protected int devPrice;
    protected static int devCount;
    protected String devCompany;
    protected String devModel;
    protected String typeOfDevOS;
    protected String randomString() {
        Random r = new Random();
        StringBuilder sb = new StringBuilder(5);
        for (int i = 0; i < 5; i++) {
            char tmp =(char) ('a' + r.nextInt('z' - 'a'));
            sb.append(tmp);
        }
        return sb.toString();
    }

    public UUID getDevId() {
        return devId;
    }

    public ElectronicDevice() {
        devId = UUID.randomUUID();
    }

    protected int randomInt() {
        return new Random().nextInt(20);
    }

}

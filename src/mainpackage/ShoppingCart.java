package mainpackage;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.UUID;

public class ShoppingCart<E extends ElectronicDevice> {
    private LinkedList<E> devices = new LinkedList<>();
    private Set<UUID> idDevice = new HashSet<>();
    public void add(E device) {
        devices.add(device);
        idDevice.add(device.getDevId());
    }
    public void delete(E device) {
        devices.remove(device);
    }
    public ElectronicDevice searchByID(UUID uuid) {
        E temp = null;
        for (E element: devices) {
            if (element.getDevId().equals(uuid)) {
                temp = element;
            }
        }
        return temp;
    }

    public void showAllObjects() {
        devices.forEach(System.out::println);
    }
}
